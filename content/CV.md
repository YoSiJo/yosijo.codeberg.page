Title: CV
Date: 2020-12-04 23:00
Modified: 2020-12-06 16:55
Category: CV
Tags: cv, publishing, job, life
Author: York-Simon Johannsen
Summary: CV of York-Simon Johannsen
Lang: en

# York-Simon Johannsen

## Main informations

* Name:         York-Simon Johannsen
* Residence:    Berlin
* Citizenship:  Germany
* E-Mail:       [info@yosijo.de](mailto:info@yosijo.de)
* XMPP:         [yosijo@jabber.waimanu.io](xmpp:yosijo@jabber.waimanu.io)

## Education

| Type of School                                                                            | Graduation                          | Time        |
|-------------------------------------------------------------------------------------------|:------------------------------------|:------------|
| Realschule,   [Freie Waldorfschule Flensburg](https://www.waldorfschule-flensburg.de/)    | Secondary school certificate        | 1996 - 2003 |
| Berufsschule, [Berufsschule des Kreises Nordfriesland in Husum](https://www.bs-husum.de/) | Vocational school certificate       | 2008 - 2011 |
| Ausbildung,   [TSBW](https://www.tsbw.de/)                                                | *IT Systems Electronics Technician* | 2008 - 2011 |

## Employment

| Employer        | Location            | Job                         | Time              |    Hours |
|-----------------|---------------------|-----------------------------|-------------------|---------:|
| Shamrock        | Flensburg,  Germany | Administration              | 2003-08 … 2006-07 | fulltime |
| CMS             | Sonderbôrg, Dansk   | Call Agent                  | 2006-08 … 2007-08 | fulltime |
| FGUE GmbH       | Flensburg,  Germany | Administration              | 2006-11 … 2007-04 | fulltime |
| Some-IT GmbH    | Berlin,     Germany | Administration              | 2010-04 … 2012-05 | fulltime |
| AVM             | Berlin,     Germany | Technical Support Agent     | 2012-09 … 2014-08 | fulltime |
| Performates UG  | Berlin,     Germany | Linux System Administratior | 2014-09 … 2015-12 | fulltime |
| Primervoto GmbH | Berlin,     Germany | Linux System Administratior | 2016-01 … 2017-09 | fulltime |
| SysEleven GmbH  | Berlin,     Germany | Linux System Enginer        | 2017-10 … 2021-10 | fulltime |
| Menzel IT       | Berlin,     Germany | Linux System Enginer        | 2021-10 … now     | fulltime |

## Applications and Services

Scale: 1 (very low) - 10 (very good).

### OS

| Operation System |  can setup                 | can configure/use           | can fix/debug              |
|:-----------------|----------------------------|-----------------------------|----------------------------|
| Debian           | [##########]         10/10 | [##########]          10/10 | [#########\_]         9/10 |
| Ubuntu           | [#########\_]         9/10 | [########\_\_]         8/10 | [########\_\_]        8/10 |
| Arch Linux       | [#########\_]         9/10 | [#########\_]          9/10 | [#########\_]         9/10 |
| CentOS           | [#######\_\_\_]       7/10 | [########\_\_]         8/10 | [#######\_\_\_]       7/10 |
| OpenWRT          | [#######\_\_\_]       7/10 | [#######\_\_\_]        7/10 | [########\_\_]        8/10 |
| Windows          | [###\_\_\_\_\_\_\_]   3/10 | [##\_\_\_\_\_\_\_\_]   2/10 | [#\_\_\_\_\_\_\_\_\_] 1/10 |

### Virtualisation

| System   |  can setup                 | can configure/use         | can fix/debug               |
|:---------|----------------------------|---------------------------|-----------------------------|
| LXC      | [########\_\_]        8/10 | [#########\_]        9/10 | [########\_\_]         8/10 |
| XEN      | [######\_\_\_\_]      6/10 | [#######\_\_\_]      7/10 | [####\_\_\_\_\_\_]     4/10 |
| KVM      | [#######\_\_\_]       7/10 | [#######\_\_\_]      7/10 | [######\_\_\_\_]       6/10 |
| Proxmox  | [#########\_]         9/10 | [#########\_]        9/10 | [#########\_]          9/10 |
| OVirt    | [##\_\_\_\_\_\_\_\_]  2/10 | [####\_\_\_\_\_\_]   4/10 | [##\_\_\_\_\_\_\_\_]   2/10 |
| libVirt  | [#######\_\_\_]       7/10 | [#######\_\_\_]      7/10 | [#####\_\_\_\_\_]      5/10 |
| Virtozzo | [####\_\_\_\_\_\_]    4/10 | [####\_\_\_\_\_\_]   4/10 | [##\_\_\_\_\_\_\_\_]   2/10 |
| Hyper-V  | [#\_\_\_\_\_\_\_\_\_] 1/10 | [##\_\_\_\_\_\_\_\_] 2/10 | [\_\_\_\_\_\_\_\_\_\_] 0/10 |


### Databases

| Database         |  can setup            | can configure/use     | can fix/debug          |
|:-----------------|-----------------------|-----------------------|------------------------|
| MySQL            | [########\_\_]   8/10 | [########\_\_]   8/10 | [########\_\_]    8/10 |
| Percona          | [########\_\_]   8/10 | [########\_\_]   8/10 | [########\_\_]    8/10 |
| MariaDB          | [########\_\_]   8/10 | [########\_\_]   8/10 | [########\_\_]    8/10 |
| PostgreSQL       | [######\_\_\_\_] 6/10 | [######\_\_\_\_] 6/10 | [######\_\_\_\_]  6/10 |
| ElasticSearch    | [######\_\_\_\_] 6/10 | [######\_\_\_\_] 6/10 | [#####\_\_\_\_\_] 5/10 |

### Automation

| Name             |  can setup              | can configure/use       | can fix/debug           |
|:-----------------|-------------------------|-------------------------|-------------------------|
| Ansible          | [#########\_]      9/10 | [########\_\_]     8/10 | [########\_\_]     8/10 |
| Salt             | [####\_\_\_\_\_\_] 4/10 | [####\_\_\_\_\_\_] 4/10 | [####\_\_\_\_\_\_] 4/10 |
| Puppet           | [#####\_\_\_\_\_]  5/10 | [########\_\_]     8/10 | [########\_\_]     8/10 |

### Monitoring

| Name    |  can setup             | can configure/use      | can fix/debug          |
|:--------|------------------------|------------------------|------------------------|
| Zabbix  | [##########]     10/10 | [#########\_]     9/10 | [#########\_]     9/10 |
| systemd | [#########\_]     9/10 | [#########\_]     9/10 | [########\_\_]    8/10 |
| Icinga  | [#####\_\_\_\_\_] 5/10 | [#####\_\_\_\_\_] 5/10 | [#####\_\_\_\_\_] 5/10 |
| Monit   | [#####\_\_\_\_\_] 5/10 | [#####\_\_\_\_\_] 5/10 | [#####\_\_\_\_\_] 5/10 |

### Programming / Scripting

| Name   | read                    | write                      | debug                      |
|:-------|-------------------------|----------------------------|----------------------------|
| bash   | [##########]      10/10 | [#########\_]         9/10 | [#########\_]         9/10 |
| python | [#####\_\_\_\_\_]  5/10 | [##\_\_\_\_\_\_\_\_]  2/10 | [##\_\_\_\_\_\_\_\_]  1/10 |
| java   | [####\_\_\_\_\_\_] 4/10 | [#\_\_\_\_\_\_\_\_\_] 1/10 | [##\_\_\_\_\_\_\_\_]  2/10 |
| php    | [#####\_\_\_\_\_]  5/10 | [##\_\_\_\_\_\_\_\_]  2/10 | [####\_\_\_\_\_\_]    4/10 |

### High level filesystems

| Filesystem | can setup             | can configure/use     | can fix/debug          |
|:-----------|-----------------------|-----------------------|------------------------|
| ZFS        | [######\_\_\_\_] 6/10 | [#######\_\_\_]  7/10 | [#####\_\_\_\_\_] 5/10 |
| btrfs      | [##########]    10/10 | [##########]    10/10 | [#######\_\_\_]   7/10 |
| GlusterFS  | [########\_\_]   8/10 | [########\_\_]   8/10 | [#######\_\_\_]   7/10 |

### Other

| Service      |  can setup                | can configure/use         | can fix/debug             |
|:-------------|:--------------------------|:--------------------------|:--------------------------|
| Apache HTTPD | [#########\_]        9/10 | [########\_\_]       8/10 | [#######\_\_\_]      7/10 |
| nginx        | [#########\_]        9/10 | [#########\_]        9/10 | [########\_\_]       8/10 |
| OpenSSH      | [#########\_]        9/10 | [########\_\_]       8/10 | [#######\_\_\_]      7/10 |
| GPG/PGP      | [########\_\_]       8/10 | [########\_\_]       8/10 | [######\_\_\_\_]     6/10 |
| PHP          | [########\_\_]       8/10 | [#########\_]        9/10 | [#######\_\_\_]      7/10 |
| Git          | [########\_\_]       8/10 | [########\_\_]       8/10 | [#######\_\_\_]      7/10 |
| GitLab       | [########\_\_]       8/10 | [#######\_\_\_]      7/10 | [######\_\_\_\_]     6/10 |
| RocketChat   | [########\_\_]       8/10 | [########\_\_]       8/10 | [######\_\_\_\_]     6/10 |
| NextCloud    | [##########]        10/10 | [##########]        10/10 | [#########\_]        9/10 |
| systemd      | [#########\_]        9/10 | [#########\_]        9/10 | [########\_\_]       8/10 |
| Netbox       | [########\_\_]       8/10 | [#########\_]        9/10 | [######\_\_\_\_]     6/10 |
| Snipe IT     | [########\_\_]       8/10 | [########\_\_]       8/10 | [######\_\_\_\_]     6/10 |

## Disclaimer

This CV is created with [Pelican](https://github.com/getpelican/pelican) and managed via the repo [yosijo.codeberg.page](https://codeberg.org/YoSiJo/yosijo.codeberg.page). The `output` diectory is a git subtree module for the [YoSiJo/pages](https://codeberg.org/YoSiJo/pages) repo.
