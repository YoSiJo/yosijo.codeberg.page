#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'York-Simon Johannsen'
SITENAME = 'York-Simon Johannsen'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'de'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
TRANSLATION_FEED_ATOM = 'feeds/all-{lang}.atom.xml'
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
  ('Pelican', 'https://getpelican.com/'),
  ('Python.org', 'https://www.python.org/'),
  ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
  ('Codeberg', '//codeberg.org/'),
)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
