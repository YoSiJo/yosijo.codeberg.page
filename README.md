# Parent repo of pages repo

## Deploy

```bash
pelican content
git commit --all --message="${_git_commit_message?}"
git push
git subtree push --prefix output git@codeberg.org:YoSiJo/pages.git main
```

## Links

1. https://codeberg.org/YoSiJo/pages
2. https://yosijo.codeberg.page/
